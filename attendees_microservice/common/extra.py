from django.views.decorators.http import require_http_methods


class Blog():
	pass

@require_http_methods(["GET", "POST"])
def list_blogs(request):
		if request.method == "GET":
				blogs = Blog.objects.all()
				response = []
				for blog in blogs:
						response.append({
								"title": blog.title,
								"href" : blog.get_api_url()
								})
				return JsonResponse({"blogs": response})

		else:
				# Convert the JSON POST request to a dictionary
				content = json.loads(request.body)

				# Use that dictionary to make a new blog post
				blog = Blog.objects.create(**content) #uses all the info in the dictionary to make a new blog instance

				# Return the new blog information
				return JsonResponse({
						"href": blog.get_api_url(),
						"title": blog.title,
						"content": blog.content,
						"comments": []
						})


@require_http_methods(["GET", "DELETE", "PUT"])
def show_blog_detail(request, id):
	if request.method == "GET":
		blog = Blog.objects.get(id=id)
		response = {
			"title": blog.title,
			"content": blog.content
        }
		return JsonResponse(response)

	elif request.method == "DELETE":
		count, _= Blog.objects.filter(id=id).delete() # this is a tuple
		return JsonResponse({"deleted": count > 0}) # returns a boolean, if deleted, true, if not false

	else:
		content = json.loads(request.body)
		Blog.objects.filter(id=id).update(**content)
		blog= Blog.objects.get(id=id)
		comments = [
			{"id": comment.id, "content": comment.content}
			for comment in blog.comments.all()
        ]
        return JsonResponse({
            "href": blog.get_api_url(),
            "title": blog.title,
            "content": blog.content,
            "comments": comments
        })
