from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_location_img(city, state):
    query = f"{city}, {state} buildings"
    url = f"https://api.pexels.com/v1/search?query={query}&per_page=1"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    img = json.loads(response.content)
    img_dicitonary = {"picture_url": img["photos"][0]["src"]["original"]}

    return img_dicitonary


def get_weather_info(city):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    location_data = json.loads(response.content)
    location_dictionary = {
        "lat": location_data[0]["lat"],
        "lon": location_data[0]["lon"],
    }
    latitude = location_dictionary["lat"]
    longitude = location_dictionary["lon"]

    url2 = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    response_2 = requests.get(url2)
    weather_data = json.loads(response_2.content)
    weather_dictionary = {
        "temp": weather_data["main"]["temp"],
        "description": weather_data["weather"][0]["description"],
    }
    return weather_dictionary
